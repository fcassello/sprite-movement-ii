﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//FRANK CASSELLO
public class sprite_mover : MonoBehaviour
{
    Transform tf;
    public float moveSpeed; // Create a variable for the degrees we move in one second
    public float turnSpeed; // Create a variable for the degrees we rotate in one second
    public bool paused;

    private Rigidbody2D rb2D;// Create a variable for our rigidbody 2D component
    private float angle = 0.0f;


    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>();
        paused = false;
        rb2D = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKey(KeyCode.P))
            Pause();

        if (paused)
            return;

        if (Input.GetKey(KeyCode.LeftShift))
        {
            if (Input.GetKeyDown(KeyCode.A))
                tf.position += new Vector3(-1.0f, 0, 0);

            if (Input.GetKey(KeyCode.D))
                tf.position += new Vector3(1, 0, 0);

            if (Input.GetKey(KeyCode.W))
                tf.Translate(tf.right * moveSpeed * Time.deltaTime, Space.World);

            if (Input.GetKey(KeyCode.X))
                tf.Translate(-tf.right * moveSpeed * Time.deltaTime, Space.World);
        }
        else
        {
            //Movement controls
            //For straffing
            /*if (Input.GetKey(KeyCode.A)) <-- Movement using Transform
                tf.position += new Vector3(-Time.time * moveSpeed, 0, 0);

            if (Input.GetKey(KeyCode.D)) <-- Movement using Transform
                tf.position += new Vector3(Time.time * moveSpeed, 0, 0);*/

            //Forward with velocity
            if (Input.GetKey(KeyCode.W))
                rb2D.velocity = transform.up * moveSpeed;

            //tf.position += new Vector3(0, Time.time * moveSpeed, 0); <-- Movement using Transform

            //Reverse with velocity
            if (Input.GetKey(KeyCode.X))
                rb2D.velocity = -transform.up * moveSpeed;

            //tf.position += new Vector3(0, -Time.time * moveSpeed, 0); <-- Movement using Transform

            // Rotate Left Script
            if (Input.GetKey(KeyCode.E))
            {
                angle -= turnSpeed * Time.deltaTime;
                tf.rotation = Quaternion.Euler(0, 0, angle);
            }
            // Rotate Right Script
            if (Input.GetKey(KeyCode.Q))
            {
                angle += turnSpeed * Time.deltaTime;
                tf.rotation = Quaternion.Euler(0, 0, angle);
            }

            if (Input.GetKey(KeyCode.Space))
                tf.position = new Vector3(0, 0, 0);
        }
    }

    public void Pause()
    {
       paused = !paused;
    }
 
}
